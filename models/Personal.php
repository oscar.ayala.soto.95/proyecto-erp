<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "personal".
 *
 * @property int $idPersona
 * @property string $nombre
 * @property string $carrera
 * @property string $direccion
 * @property string $colonia
 * @property int $telefono
 * @property int $idDepartamento
 *
 * @property Departamentos $departamentos
 */
class Personal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'carrera', 'direccion', 'colonia', 'telefono', 'idDepartamento'], 'required'],
            [['telefono', 'idDepartamento'], 'integer'],
            [['nombre', 'carrera', 'direccion', 'colonia'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idPersona' => 'Id Persona',
            'nombre' => 'Nombre',
            'carrera' => 'Carrera',
            'direccion' => 'Direccion',
            'colonia' => 'Colonia',
            'telefono' => 'Telefono',
            'idDepartamento' => 'Id Departamento',
        ];
    }

    /**
     * Gets query for [[Departamentos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDepartamentos()
    {
        return $this->hasOne(Departamentos::className(), ['idDepartamento' => 'idDepartamento']);
    }
}
