<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "campus".
 *
 * @property int $idCampus
 * @property string $campus
 * @property int $idDepartamento
 *
 * @property Departamentos $idDepartamento0
 */
class Campus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'campus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['campus', 'idDepartamento'], 'required'],
            [['idDepartamento'], 'integer'],
            [['campus'], 'string', 'max' => 100],
            [['idDepartamento'], 'exist', 'skipOnError' => true, 'targetClass' => Departamentos::className(), 'targetAttribute' => ['idDepartamento' => 'idDepartamento']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idCampus' => 'Id Campus',
            'campus' => 'Campus',
            'idDepartamento' => 'Id Departamento',
        ];
    }

    /**
     * Gets query for [[IdDepartamento0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdDepartamento0()
    {
        return $this->hasOne(Departamentos::className(), ['idDepartamento' => 'idDepartamento']);
    }
}
