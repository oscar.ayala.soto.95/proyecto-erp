<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "departamentos".
 *
 * @property int $idDepartamento
 * @property string $departamento
 *
 * @property Campus[] $campuses
 * @property Personal $idDepartamento0
 */
class Departamentos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'departamentos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['departamento'], 'required'],
            [['departamento'], 'string', 'max' => 100],
            [['idDepartamento'], 'exist', 'skipOnError' => true, 'targetClass' => Personal::className(), 'targetAttribute' => ['idDepartamento' => 'idDepartamento']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idDepartamento' => 'Id Departamento',
            'departamento' => 'Departamento',
        ];
    }

    /**
     * Gets query for [[Campuses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCampuses()
    {
        return $this->hasMany(Campus::className(), ['idDepartamento' => 'idDepartamento']);
    }

    /**
     * Gets query for [[IdDepartamento0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdDepartamento0()
    {
        return $this->hasOne(Personal::className(), ['idDepartamento' => 'idDepartamento']);
    }
}
