<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\dropDownList;
use app\models\Departamentos;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Campus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="campus-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'campus')->textInput(['maxlength' => true]) ?>

    <?= 
   $form->field($model, 'idDepartamento')->dropDownList(
       ArrayHelper::map(Departamentos::find()->all(), 'idDepartamento', 'departamento'),['prompt' => 
       'Seleccione Departamento',]
   ) ?>


    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
